<?php

namespace Outctrl\Foundation\Bus;

use Carbon\Carbon;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Outctrl\Foundation\Traits\Validation;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class AbstractBus
 * @package Outctrl\Foundation\Abstracts
 */
abstract class AbstractBus
{
    use Validation;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Request
     */
    protected $request;

    /**
     * AbstractBus constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->request = $this->app(Request::class);

        $this->getRouteParams();
        $this->request->merge($this->prepare());
        $this->initValidation($container);
        $this->boot();
    }

    /**
     * @param null $make
     * @param array $parameters
     * @return mixed override make object
     */
    protected function app($make = null, array $parameters = [])
    {
        if ($make === null) {
            return $this->container;
        }

        return $this->container->make($make, $parameters);
    }

    private function getRouteParams()
    {
        $resolver = $this->request->getRouteResolver()();

        if (isset($resolver)) {
            $this->request->merge($resolver[2]);
        }
    }

    /**
     * @return array
     */
    protected function prepare(): array
    {
        return [];
    }

    abstract protected function boot();

    abstract public function handle();

    abstract protected function user();

    /**
     * @param $key
     * @param null $default
     * @return Carbon|null
     */
    protected function getAsCarbon($key, $default = null): ?Carbon
    {
        return ($this->has($key))
            ? Carbon::parse($this->get($key), config('app.timezone', 'UTC'))
            : $default;
    }

    /**
     * @param $key
     * @return bool
     */
    protected function has($key): bool
    {
        return $this->request->has($key);
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    protected function get($key, $default = null)
    {
        return $this->request->get($key, $default);
    }

    /**
     * @param $key
     * @param null $default
     * @return UploadedFile
     */
    protected function file($key, $default = null): UploadedFile
    {
        return $this->request->files->get($key, $default);
    }

    /**
     * @return array
     */
    protected function all(): array
    {
        return $this->request->all();
    }

    /**
     * @param array $keys
     * @return array
     */
    protected function only(...$keys): array
    {
        return $this->request->only($keys);
    }

    /**
     * @param array $allows
     */
    protected function notAllowed($allows = [])
    {
        throw new MethodNotAllowedHttpException($allows);
    }

    /**
     * @throws NotFoundHttpException
     */
    protected function notFound()
    {
        throw new NotFoundHttpException();
    }
}
