<?php

namespace Outctrl\Foundation\Resource;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\ResourceResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class AbstractResourceCollection
 * @package Outctrl\Foundation\Resource
 */
abstract class AbstractResourceCollection extends ResourceCollection
{
    /**
     * @var Collection
     */
    public $resource;

    public function __construct($resource)
    {
        if (is_array($resource)) {
            $resource = new Collection($resource);
        }
        parent::__construct($resource);
    }

    /**
     * Transform the resource into a JSON array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof LengthAwarePaginator) {
            return [
                'data' => $this->convertResource($request),
                'meta' => [
                    'page' => $this->resource->currentPage(),
                    'last_page' => $this->resource->lastPage(),
                    'limit' => $this->resource->perPage(),
                    'total' => $this->resource->total(),
                    'first' => $this->resource->firstItem(),
                    'last' => $this->resource->lastItem()
                ]
            ];
        }

        return $this->convertResource($request);
    }

    /**
     * @param $request
     * @return array
     */
    protected function convertResource($request): array
    {
        if (is_array($this->collection[0] ?? null)) {
            return $this->collection->all();
        }

        return $this->collection->map->toArray($request)->all();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function toResponse($request): JsonResponse
    {
        return (new ResourceResponse($this))->toResponse($request);
    }
}
