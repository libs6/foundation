<?php

namespace Outctrl\Foundation\Traits;

use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

/**
 * Trait Validation
 * @package Outctrl\Foundation\Traits
 */
trait Validation
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var MessageBag
     */
    private $messageBag;

    /**
     * @param Container $container
     */
    protected function initValidation(Container $container)
    {
        $this->validator = $container->get('validator')->make([], []);
        $this->validator->setData($container->get(Request::class)->all());
        $this->messageBag = new MessageBag();
    }

    /**
     * @throws ValidationException
     */
    protected function validate()
    {
        if ($this->fails()) {
            $this->validator->messages()->merge($this->messageBag);
            throw new ValidationException($this->validator);
        }
    }

    /**
     * @return bool
     */
    protected function fails(): bool
    {
        return $this->validator->fails() || $this->messageBag->isNotEmpty();
    }

    /**
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @return $this
     */
    protected function addValidation(array $rules, array $messages = [], array $customAttributes = []): Validation
    {
        $this->validator
            ->setRules($rules)
            ->setCustomMessages($messages)
            ->addCustomAttributes($customAttributes);

        return $this;
    }

    /**
     * @param $attribute
     * @param string|null $id
     * @param array $replacer
     * @param null $locale
     * @return $this
     */
    protected function addError($attribute, $id, $replacer = [], $locale = null): Validation
    {
        $this->validator->setRules([]);
        $this->messageBag->add($attribute, trans($id, $replacer, $locale));

        return $this;
    }

    /**
     * @return Validator
     */
    protected function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @return MessageBag
     */
    protected function getMessages(): MessageBag
    {
        return $this->messageBag;
    }

    /**
     * @param MessageBag $bag
     */
    protected function mergeErrors(MessageBag $bag)
    {
        $this->validator->messages()->merge($bag);
    }
}
