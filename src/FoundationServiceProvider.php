<?php

namespace Outctrl\Foundation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;

/**
 * Class FoundationServiceProvider
 * @package Outctrl\Foundation
 */
class FoundationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->configureTranslator();
    }

    protected function configureTranslator()
    {
        $this->app->singleton('translation.loader', function ($app) {
            return new FileLoader($app['files'], base_path('resources/lang'));
        });

        $this->app->singleton('translator', function ($app) {
            $loader = $app['translation.loader'];
            $locale = $app['config']['app.locale'];

            $trans = new Translator($loader, $locale);
            $trans->setFallback($app['config']['app.fallback_locale']);

            return $trans;
        });
    }
}
